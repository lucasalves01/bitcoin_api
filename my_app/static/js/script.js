$(function() {
  	// var user = $('#txtUsername').val();
   //  var pass = $('#txtPassword').val();
    $.ajax({
        url: '/dashboard/',
        data: $('form').serialize(),
        type: 'POST',
        success: function(response) {
        	response = JSON.parse(response);
        	if (response.status == 'OK'){
        		//Configuração do gráfico de barra.
        		$('#div_home').css('display', 'block');
        		var months = response.months;
				var values = response.values;
				var data = [
							{
								x: months,
								y: values,
								type: 'bar'
							}
							];
				var layout = {
				  title: 'Volume de negociações mensais em 2016',
				  font:{
				    family: 'Raleway, snas-serif'
				  },
				  showlegend: false,
				  xaxis: {
				    tickangle: -45
				  },
				  yaxis: {
				    zeroline: false,
				    gridwidth: 2
				  },
				  bargap :0.05
				};
				Plotly.newPlot('graphDiv', data, layout);

				//Oculta a div da mensagem de loading.
				$( "#msg_inital" ).hide();

				//Configura as divs com os valores analíticos.
				$('#total_volume_reais').append(response.total_volume_reais);
				$('#total_negociacoes_ano').append(response.total_negociacoes_ano);
				$('#media_diaria_volume_reais').append(response.media_diaria_volume_reais);
				$('#media_diaria_negociacoes_ano').append(response.media_diaria_negociacoes_ano);

        	}
        	else{
        		
				
        	}
            
        },
        error: function(error) {
            console.log(error);
        }
    });
});

