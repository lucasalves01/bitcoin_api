#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from flask import request, jsonify, Blueprint, abort
from flask.views import MethodView
from my_app import db, app
from my_app.product.models import BitcoinDaySummary
from datetime import timedelta, date
import requests
import time
from flask import Flask, render_template
import locale
from datetime import datetime
import operator

from flask import Flask, redirect, url_for



product = Blueprint('product', __name__)
 
@product.route('/')
@product.route('/home')
def home():
    return "Welcome to the BitcoinDaySummary Home."
 
 
class Dashboard(MethodView):
    '''
        View para buscar os dados analíticos
    '''
    def daterange(self,start_date, end_date):
        '''
            Recebe uma data de inicio e fim e retorno o período.
        '''
        for n in range(int ((end_date - start_date).days)):
            yield start_date + timedelta(n)

    def requestdate(self,date_):
        '''
            Recebe a data e faz a requisição na API.
            A API retorna um json.
        '''
        time.sleep(2.5) #Tempo de 2,5 segundos para não exceder o limite de requisição da API.
        url = ('https://www.mercadobitcoin.net/api/BTC/day-summary/{}/{}/{}/').format(date_.year,date_.month,date_.day)
        req =  requests.get(url)
        print(url)
        print(req)
        print(datetime.now())
        print('----')
        json_fiels = req.json()
        opening = json_fiels['opening']
        closing = json_fiels['closing']
        lowest = json_fiels['lowest']
        highest = json_fiels['highest']
        volume = json_fiels['volume']
        quantity = json_fiels['quantity']
        amount = json_fiels['amount']
        avg_price = json_fiels['avg_price']

        return opening,closing,lowest,highest,volume,quantity,amount,avg_price
    def analytics(self,bitcoins):
        '''
            Recebe a lista de todos os objetos e faz os cálculos para as análises propostas.
            Retorna um json para cada item a ser exibido
        '''
        volume_reais_ano = 0
        numero_negociacoes_ano = 0
        media_real_dia = 0
        media_negociacoes_dia = 0
        month_old = 0
        negociacoes_month = 0
        dict_month = {}
        for bitcoin in bitcoins:
            volume_reais_ano = volume_reais_ano + bitcoin.volume
            numero_negociacoes_ano = numero_negociacoes_ano + bitcoin.amount
            media_real_dia = media_real_dia + bitcoin.avg_price

            date_ = datetime.strptime(bitcoin.date, '%Y-%m-%d')

            if date_.month == month_old:
                negociacoes_month = negociacoes_month+bitcoin.amount
                dict_month.update({date_.strftime("%b"):negociacoes_month})
            else:
                negociacoes_month = 0
                negociacoes_month = bitcoin.amount
                month_old = date_.month
                dict_month.update({date_.strftime("%b"):negociacoes_month})
        media_real_dia = media_real_dia / len(bitcoins)
        media_negociacoes_dia = numero_negociacoes_ano / len(bitcoins)
        locale.setlocale( locale.LC_ALL, '' )
        volume_reais_format = locale.currency(volume_reais_ano, grouping=True )
        media_real_dia = locale.currency(media_real_dia, grouping=True )
        print("Total de volume de Reais (BRL) negociados neste ano: {}".format(volume_reais_format))
        print("Total de negociacoes realizadas neste ano: {}".format(numero_negociacoes_ano))
        print("Media diaria de volume de Reais (BRL) negociados neste ano: {}".format(media_real_dia))
        print("Media diaria de negociacoes neste ano: {}".format(media_negociacoes_dia))


        months = []
        values = []

        dict_month = sorted(dict_month.iteritems(), key=operator.itemgetter(1), reverse=True)
        for month_ in dict_month:
            months.append(month_[0])
            values.append(month_[1])

        return json.dumps({'status':'OK','months':months,'values':values,
                            'total_volume_reais': volume_reais_format,
                            'total_negociacoes_ano':numero_negociacoes_ano,
                            'media_diaria_volume_reais':media_real_dia,
                            'media_diaria_negociacoes_ano':media_negociacoes_dia});    


    def get(self):
        '''
            Método GET da view retorna o template a ser renderizado.
        '''
        return render_template('index.html')
    
    def post(self, id=None, page=1):
        '''
            Método POST utilizado para retornar os dados analíticos feitos via requisição AJAX.
        '''
        bitcoins = BitcoinDaySummary.query.order_by(BitcoinDaySummary.date).all()
        if len(bitcoins)>=365:
            '''
                Se a base já esta populada, retorna os dados calculados.
            '''
            json_analytics = self.analytics(bitcoins)
            return json_analytics
          
        else:
            '''
                Se ainda não tem dados na base ou está incompleta, realiza o processo de requisição na API, estruturando a base de dados
                e faz os cálculos necessários, e redirecionando para a url do template inicial.
            '''
            start_date = date(2016, 1, 1)
            end_date = date(2016, 12, 31)
            for single_date in self.daterange(start_date, end_date):
                s_date = single_date.strftime("%Y-%m-%d")
                exists = db.session.query(db.exists().where(BitcoinDaySummary.date == s_date)).scalar()
                if not exists:
                    try:
                        opening,closing,lowest,highest,volume,quantity,amount,avg_price = self.requestdate(single_date)
                    except:
                        time.sleep(60)
                        opening,closing,lowest,highest,volume,quantity,amount,avg_price = self.requestdate(single_date)


                    bitcoinday = BitcoinDaySummary(s_date,opening,closing,lowest,highest,volume,quantity,amount,avg_price)
                    db.session.add(bitcoinday)
                    db.session.commit()

            bitcoins = BitcoinDaySummary.query.order_by(BitcoinDaySummary.date).all()
            json_analytics = self.analytics(bitcoins)
            return json_analytics

            return redirect(url_for('Dashboard'))



dashboard =  Dashboard.as_view('Dashboard')
app.add_url_rule(
    '/dashboard/', view_func=dashboard, methods=['GET','POST']
)