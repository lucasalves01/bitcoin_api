#!/usr/bin/env python
# -*- coding: utf-8 -*-

from my_app import db
 
class BitcoinDaySummary(db.Model):
	'''
		Model para o resumo diário de bitcoin
	'''
	id = db.Column(db.Integer,primary_key=True)
	date = db.Column(db.String(50))
	opening = db.Column(db.Float)
	closing = db.Column(db.Float)
	lowest = db.Column(db.Float)
	highest = db.Column(db.Float)
	volume = db.Column(db.Float)
	quantity = db.Column(db.Float)
	amount = db.Column(db.Integer)
	avg_price = db.Column(db.Float)

 
	def __init__(self,date,opening,closing,lowest,highest,volume,quantity,amount,avg_price):
		self.date = date
		self.opening = opening
		self.closing = closing
		self.lowest = lowest
		self.highest = highest
		self.volume = volume
		self.quantity = quantity
		self.amount = amount
		self.avg_price = avg_price
 
	def __repr__(self):
		return '<BitcoinDaySummary %d>' % self.id