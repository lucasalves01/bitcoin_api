# Análises de movimentação do Bitcoin em 2016
##Descrição

Esse projeto visa consumir uma API do https://www.mercadobitcoin.com.br/api-doc/ para analisar as transações em valores e quantitativas que ocorrem no ano de 2016.


##Como instalar
	* Faça o clone do repositório.
	* pip install -r requeriments.txt

##Executando
	* python run.py
	* acesse: https://localhost:5000/dashboard
	* Na primeira vez, ainda não temos a base populada pelos dados da API. Observe no terminal um log das requisições 
	que estão sendo feitas. Durante esse processo, a página fica mostrando um "loading" até que os dados estejam prontos.
	
##Observações
	* Caso precisem da base pronta, solicite-a pelo email lucasalves.s@outlook.com


